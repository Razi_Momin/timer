import React, { useState, useEffect } from 'react';
// import './App.css';

function TimerHelper(props) {
    //console.log(props);
    const { time, color_Array } = props;
    const [timerDays, setTimerDays] = useState('');
    const [timerHours, setTimerHours] = useState('');
    const [timerMinutes, setTimerMinutes] = useState('');
    const [timerSeconds, setTimerSeconds] = useState('');
    const [timerStatus, setTimerStatus] = useState(true);
    let interval;
    const startTimer = () => {
        const countdownDate = new Date(time).getTime();
        interval = setInterval(() => {
            const now = new Date().getTime();
            const distance = countdownDate - now;
            let days = Math.floor(distance / (1000 * 60 * 60 * 24));
            let hours = Math.floor((distance % (1000 * 60 * 60 * 24) / (1000 * 60 * 60)));
            let minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
            let seconds = Math.floor((distance % (1000 * 60)) / 1000);
            if (distance < 0) {
                //stop timer
                setTimerStatus(false);
                clearInterval(interval.current);
            } else {
                // update timer
                days = (days > 0) ? "D:" + days + " " : '';
                hours = (hours > 0) ? "H:" + hours + " " : '';
                minutes = (minutes > 0) ? "M:" + minutes + " " : '';
                seconds = (seconds > 0) ? "S" + seconds : '';
                setTimerDays(days);
                setTimerHours(hours);
                setTimerMinutes(minutes);
                setTimerSeconds(seconds);
            }
        }, 1000);
    }
    useEffect(() => {
        startTimer();
        return () => {
            clearInterval(interval.current);
        };
    },[])
    return (
        <span>
            Count down timer
            {
                (!timerStatus) ? 'Live' :
                    <span><span style={{ color: color_Array[0] }}>{timerDays}</span> {timerHours + timerMinutes + timerSeconds}</span>
            }
        </span>
    );
}

export default TimerHelper;
