import React, { useState, useRef, useEffect } from 'react';
// import './App.css';
import TimeHelper from './TimeHelper';

function App() {
  const [colorArray, setColorArray] = useState(['red', 'yellow', 'green', 'blue']);
  // const [timerDays, setTimerDays] = useState('');
  // const [timerHours, setTimerHours] = useState('');
  // const [timerMinutes, setTimerMinutes] = useState('');
  // const [timerSeconds, setTimerSeconds] = useState('');
  // let interval = useRef();
  // const startTimer = () => {
  //   const countdownDate = new Date('2020-06-19 13:30:00').getTime();
  //   interval = setInterval(() => {
  //     const now = new Date().getTime();
  //     const distance = countdownDate - now;
  //     let days = Math.floor(distance / (1000 * 60 * 60 * 24));
  //     let hours = Math.floor((distance % (1000 * 60 * 60 * 24) / (1000 * 60 * 60)));
  //     let minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
  //     let seconds = Math.floor((distance % (1000 * 60)) / 1000);
  //     if (distance < 0) {
  //       //stop timer

  //       clearInterval(interval.current);
  //     } else {
  //       // update timer
  //       days = (days > 0) ? days : '';
  //       hours = (hours > 0) ? hours : '';
  //       minutes = (minutes > 0) ? minutes : '';
  //       seconds = (seconds > 0) ? seconds : '';
  //       if(days == 0 && hours == 0 && minutes == 0 && seconds == 0){

  //       }
  //       setTimerDays(days);
  //       setTimerHours(hours);
  //       setTimerMinutes(minutes);
  //       setTimerSeconds(seconds);
  //     }
  //   }, 1000)
  // }
  // useEffect(() => {
  //   startTimer();
  //   return () => {
  //     clearInterval(interval.current);
  //   };
  // })
  return (
    <div>
      <TimeHelper time="2020-06-18 20:10:00" color_Array={colorArray} />
    </div>
    // <section className="timer-container">
    //   <section className="timer">
    //     <div>
    //       <span className="mdi mdi-calendar-clock timer-icon"></span>
    //       <h2>Count down timer</h2>
    //       <p>countdown is really special date.</p>
    //     </div>
    //     <div>
    //       <section>
    //         <p>{timerDays}</p>
    //         <p><small>Days</small></p>
    //       </section>
    //       <span>:</span>
    //       <section>
    //         <p>{timerHours}</p>
    //         <p><small>Hours</small></p>
    //       </section>
    //       <span>:</span>
    //       <section>
    //         <p>{timerMinutes}</p>
    //         <p><small>Minutes</small></p>
    //       </section>
    //       <span>:</span>
    //       <section>
    //         <p>{timerSeconds}</p>
    //         <p><small>Seconds</small></p>
    //       </section>
    //       <span>:</span>
    //     </div>
    //   </section>
    // </section>

  );
}

export default App;
